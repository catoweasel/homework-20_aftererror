// Fill out your copyright notice in the Description page of Project Settings.


#include "TurnBlock.h"
#include "SnakeBase.h"

// Sets default values
ATurnBlock::ATurnBlock()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ATurnBlock::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATurnBlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATurnBlock::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		ATurnBlock* CatchedTurnBlock = Cast<ATurnBlock>(this);
		int32 RandomDirection = FMath::RandRange(0, 1);
		
		if (IsValid(Snake))
		{
			if (Snake->LastMoveDirection != EMovementDirection::UP || Snake->LastMoveDirection != EMovementDirection::DOWN)
			{
				if (RandomDirection == 0)
				{
					Snake->LastMoveDirection = EMovementDirection::RIGHT;
				}

				else
				{
					Snake->LastMoveDirection = EMovementDirection::LEFT;
				}
			}

			else if (Snake->LastMoveDirection != EMovementDirection::LEFT || Snake->LastMoveDirection != EMovementDirection::RIGHT)
			{
				if (RandomDirection == 0)
				{
					Snake->LastMoveDirection = EMovementDirection::UP;
				}

				else
				{
					Snake->LastMoveDirection = EMovementDirection::DOWN;
				}
			}
		}

		if (IsValid(Snake))
		{
			if (IsValid(CatchedTurnBlock))
			{
				CatchedTurnBlock->Destroy();
				Snake->RemoveTurnBlock();
			}
		}
	}
}