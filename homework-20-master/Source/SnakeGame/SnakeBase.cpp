// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Interactable.h"
#include "Food.h"
#include "SuperFood.h"
#include "TurnBlock.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickInterval(MovementSpeed); 
	AddSnakeElement(5);

	SpawnFood(2);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}



void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		int32 ElementIndex = SnakeElements.Add(NewSnakeElement);

		//SwitchVisibility();

	        if (SnakeElements.Num() > 5)
		{
			SnakeElements.Last()->MeshComponent->SetVisibility(false);
		}
		
		if (ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
			//NewSnakeElement->MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
		}
	}
}



void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	//AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements.Last()->MeshComponent->SetVisibility(true); 
	SnakeElements.Last(1)->MeshComponent->SetVisibility(true);
	SnakeElements.Last(2)->MeshComponent->SetVisibility(true); //�� ����, ��� ������� ��������� ��������� � ���������

	SnakeElements[0]->AddActorWorldOffset(MovementVector); //SetActorLocation(GetActorLocation()+MovementVector)
	SnakeElements[0]->ToggleCollision();
}



void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);

			IInteractable* CathedFood = Cast<AFood>(Other);
			if(CathedFood)
			{
				SpawnFood();
			}
		}
	}
}



void ASnakeBase::SwitchVisibility()
{
	GetWorldTimerManager().SetTimer(VisibilityTimerHandle, this, &ASnakeBase::SwitchVisibility, MovementSpeed, true);

	if (--Countdown >= 0)
	{
		if (SnakeElements.Num() > 5)
		{
			SnakeElements.Last()->MeshComponent->ToggleVisibility();
		}
	}
	
	else
	{
		Countdown = 2;
		GetWorldTimerManager().ClearTimer(VisibilityTimerHandle);
	}

	//�������
}



void ASnakeBase::SpawnFood(int FoodAmount)
{
	for (int i = 0; i < FoodAmount; ++i)
	{
		int32 RandomLocationX = FMath::RandRange(-940, 940);
		int32 RandomLocationY = FMath::RandRange(-940, 940);
		FVector NewLocation(RandomLocationX, RandomLocationY, 0);
		FTransform NewTransform(NewLocation);
		//AFood* NewFoodElement = GetWorld()->SpawnActor<AFood>(FoodElementClass, NewTransform);
		GetWorld()->SpawnActor<AFood>(FoodElementClass, NewTransform); //��������?

		SpawnSuperFood();
		SpawnTurnBlock();
	}
}



void ASnakeBase::SpawnSuperFood()
{
	if (SnakeElements.Num() != 5 && SnakeElements.Num() % 5 == 0 && SuperFoodElements.Num() < 1)
	{
		int32 RandomLocationX = FMath::RandRange(-940, 940);
		int32 RandomLocationY = FMath::RandRange(-940, 940);
		FVector NewLocation(RandomLocationX, RandomLocationY, 0);
		FTransform NewTransform(NewLocation);
		ASuperFood* NewSuperFoodElement = GetWorld()->SpawnActor<ASuperFood>(SuperFoodElementClass, NewTransform);
		SuperFoodElements.Add(NewSuperFoodElement);

		/*for (int i = SuperFoodElements.Num() - 1; i > 0; i--)
		{
			GetWorldTimerManager().SetTimer(SuperFoodTimerHandle, this, &ASnakeBase::SwitchVisibility, 3, true, 3);
			
			int32 NewRandomLocationX = FMath::RandRange(-940, 940);
			int32 NewRandomLocationY = FMath::RandRange(-940, 940);
			FVector Jump(NewRandomLocationX, NewRandomLocationY, 0);
			auto CurrentElement = SuperFoodElements[i];
			CurrentElement->SetActorLocation(Jump);
		}*/
		
		JumpSuperFood();
	}
}

void ASnakeBase::JumpSuperFood()
{
	//GetWorldTimerManager().SetTimer(SuperFoodTimerHandle, this, &ASnakeBase::SwitchVisibility, 3, true, 3);

	if (IsValid(SuperFoodElementClass))
	{
		for (int i = SuperFoodElements.Num() - 1; i >= 0; i--)
		{
			GetWorldTimerManager().SetTimer(SuperFoodTimerHandle, this, &ASnakeBase::JumpSuperFood, 5, true, 5);

			int32 NewRandomLocationX = FMath::RandRange(-940, 940);
			int32 NewRandomLocationY = FMath::RandRange(-940, 940);
			FVector Jump(NewRandomLocationX, NewRandomLocationY, 0);
			auto CurrentElement = SuperFoodElements[i];
			CurrentElement->SetActorLocation(Jump);
		}
	}
	else
	{
	GetWorldTimerManager().ClearTimer(SuperFoodTimerHandle);
	}
}

void ASnakeBase::RemoveSuperFoodElement()
{
	SuperFoodElements.RemoveAt(0);
}



void ASnakeBase::SpawnTurnBlock()
{
	if (SnakeElements.Num() % 7 == 0)
	{
		int32 RandomLocationX = FMath::RandRange(-940, 940);
		int32 RandomLocationY = FMath::RandRange(-940, 940);
		FVector NewLocation(RandomLocationX, RandomLocationY, 0);
		FTransform NewTransform(NewLocation);
		ATurnBlock* NewTurnBlockElement = GetWorld()->SpawnActor<ATurnBlock>(TurnBlockElementClass, NewTransform);
		TurnBlockElements.Add(NewTurnBlockElement);

		DestroyOldTurnBlock();
	}
}

void ASnakeBase::DestroyOldTurnBlock()
{
	/*if (IsValid(TurnBlockElementClass))
	{
		GetWorldTimerManager().SetTimer(VisibilityTimerHandle, this, &ASnakeBase::DestroyOldTurnBlock, 1, true, 1);

		int32 SecCounterTurnBlock[TurnBlockElements.Num()];
		for (int i = TurnBlockElements.Num() - 1; i >= 0; i--)
		{
			auto CurrentElement = TurnBlockElements[i];
			int Counter(&i) = 0;
			Counter(*i) += 1;
			CurrentElement->;
		}
	}
	
	else
	{
		GetWorldTimerManager().ClearTimer(SuperFoodTimerHandle);
	}*/
}

void ASnakeBase::RemoveTurnBlock()
{
	TurnBlockElements.RemoveAt(0);
}
