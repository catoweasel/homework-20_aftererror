// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class AFood;
class ASuperFood;
class ATurnBlock;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodElementClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASuperFood> SuperFoodElementClass;
	
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ATurnBlock> TurnBlockElementClass;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
		TArray<ASuperFood*> SuperFoodElements;

	UPROPERTY()
		TArray<ATurnBlock*> TurnBlockElements;

	UPROPERTY()
		EMovementDirection LastMoveDirection;

	UPROPERTY()
		int32 Countdown = 2;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void AddSnakeElement(int ElementsNum = 1);

	UFUNCTION(BlueprintCallable)
		void Move();

	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	FTimerHandle VisibilityTimerHandle;

	FTimerHandle SuperFoodTimerHandle;

	UFUNCTION()
		void SwitchVisibility();

	UFUNCTION()
		void SpawnFood(int FoodAmount = 1);

	UFUNCTION()
		void SpawnSuperFood();

	UFUNCTION()
		void JumpSuperFood();

	UFUNCTION()
		void RemoveSuperFoodElement();

	UFUNCTION()
		void SpawnTurnBlock();

	UFUNCTION()
		void DestroyOldTurnBlock();

	UFUNCTION()
		void RemoveTurnBlock();
};
