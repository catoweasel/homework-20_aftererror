// Fill out your copyright notice in the Description page of Project Settings.


#include "SuperFood.h"
#include "SnakeBase.h"

// Sets default values
ASuperFood::ASuperFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ASuperFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASuperFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASuperFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		ASuperFood* CatchedFood = Cast<ASuperFood>(this);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement(2);

			if (IsValid(CatchedFood))
			{
				CatchedFood->Destroy();
				Snake->RemoveSuperFoodElement();
			}
		}
	}
}

