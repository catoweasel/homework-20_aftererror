// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();

	//SpawnFood();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		//auto Food = Cast<AFood>(Interactor);
		AFood* CatchedFood = Cast<AFood>(this);
		if(IsValid(Snake))
		{
			Snake->AddSnakeElement();
			//SpawnFood();
			
			if (IsValid(CatchedFood))
			{
				//SpawnFood();
				CatchedFood->Destroy();
			}
		}
	}
}

/*void AFood::SpawnFood(int RandomElements)
{
	//int RandomElements = FMath::RandRange(1, 3);
	for (int i = 0; i < RandomElements; ++i)
	{
		int32 RandomLocationX = FMath::RandRange(-940, 940);
		int32 RandomLocationY = FMath::RandRange(-940, 940);
		FVector NewLocation(RandomLocationX, RandomLocationY, 0);
		FTransform NewTransform(NewLocation);
		AFood* NewFoodElement = GetWorld()->SpawnActor<AFood>(FoodElementClass, NewTransform);
		//AFood* NewFoodElement = GetWorld()->SpawnActor(AFood::FoodElementClass, NewTransform);
		//GetWorld()->SpawnActor<AFood>(FoodElementClass, NewTransform);
	}
}*/
